Tools
=========

Utilisé généralement pour installer un load balancer (traefik), ainsi qu'un gestionnaire de certificats (acme). Cependant l'infrastructure de l'ecam ne le permet pas. 

Nous allons installer uniquement Portainer qui est utilisé pour la gestion, la maintenance et le dépannage de docker.

Remarques
------------

Comme nous n'avons pas de load balancer, les accès aux différents services ne se feront pas via un nom de domaine, mais via leur port

License
-------

BSD

Author informations
------------------

Birchler Arno ECAM6 @2021 (arno.birchler@viacesi.fr)
