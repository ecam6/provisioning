Swarm cluster
=========

- Reset des données de swarm (dans le cas où le serveur était déjà configurer en swarm)
- Passage du leader en mode swarm
- Ajout des manager au swarm (si plusieurs servers en mode manager)
- Ajout des workers au swarm (si plusieurs servers en mode worker)


License
-------

BSD

Birchler Arno
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
