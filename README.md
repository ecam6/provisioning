# Installation

## Dépendances

Docker doit être installé sur la machine qui exécutera le provisioning.

# Configuration

## Hosts
Avant de lancer ce playbook vous devez vérifier le fichier `hosts`. (ex : `hosts.sample`) Ce fichier permet de mapper le nom des machines à l'ip fixe voulue.

Attention le nom donner dans ce fichier sera le nom de la VM.

## Vars

Vous pouvez aussi vérifier le fichier `all.yml` situé dans le dossier `group_vars`. Ce fichier définit toutes les variables nécessaire au fonctionnement du provisioning. Il est recommandé de toucher à rien si vous n'êtes pas pleinement conscient des conséquences.


# Lancement du provisioning

## Linux

`./start_env_linux.sh`

## Windows
`.\start_env_win.bat`

Attention les serveurs de l'ecam bloque l'accès au PPA keyserver.ubuntu.com

Il faut utiliser une autre connexion pour réussir à construire l'image

## Général

Une fois l'application construite et lancée, un choix vous sera demandé.

Si c'est la première fois que vous lancer la recette de provisioning vous devez lancer toutes les recettes.



# Execution

Pour lancer la recette vous devez lancer le fichier `start.sh` et possédé la clé vault dans le fichier `~/.vault_pass.txt`

# Remarques

Si vous voulez changer le mot de passe d'accès au serveur, vous devez chiffré le mot de passe avec l'outil suivant :
`ansible-vault encrypt_string`
Copier la sortie de la commande puis la coller dans le fichier `hosts.yml` au niveau du mot de passe à changer.