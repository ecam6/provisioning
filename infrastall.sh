#/bin/bash

start_cmd="ansible-playbook -i hosts.yml --ask-vault-pass"
export PYTHONWARNINGS=ignore::UserWarning

echo "==== Welcome to Infrastall ===="
echo "! Before doing anythink please check group_vars directory !"
echo "== Main menu =="
echo "10 - Setup all servers"
echo "20 - Create a Docker Swarm Cluster"
echo "21 - Leave Swarm cluster"
echo "30 - Deploy Stack Tools"
echo "31 - Undeploy Stack Tools"
echo "40 - Deploy Stack Pick to light"
echo "41 - Undeploy Stack Pick to light"
echo "42 - First Boot Pick to light (migrate & seed)"
echo "=== @ Birchler Arno ==="

read choice

if [ "$choice" = "10" ]
then
    echo "Starting playbook <10 - Setup all servers>"
    $start_cmd 01-configure-server.yml
elif [ "$choice" = "20" ]
then
    echo "Starting playbook <20 - Create a Docker Swarm Cluster>"
    $start_cmd 02-swarm-cluster.yml --extra-vars "cond_action=deploy"
elif [ "$choice" = "21" ]
then
    echo "Starting playbook <21 - Leave Swarm cluster>"
    $start_cmd 02-swarm-cluster.yml --extra-vars "cond_action=undeploy"
#----
elif [ "$choice" = "30" ]
then
    echo "Starting playbook <30 - Deploy Stack Tools>"
    $start_cmd 03-stack-tools.yml --extra-vars "cond_action=deploy"
elif [ "$choice" = "31" ]
then
    echo "Starting playbook <31 - Undeploy Stack Tools>"
    $start_cmd 03-stack-tools.yml --extra-vars "cond_action=undeploy"
#----
elif [ "$choice" = "40" ]
then
    echo "Starting playbook <40 - Deploy Stack Pick to light>"
    $start_cmd 04-stack-ptl.yml --extra-vars "cond_action=deploy"
elif [ "$choice" = "41" ]
then
    echo "Starting playbook <41 - Undeploy Stack Pick to light>"
    $start_cmd 04-stack-ptl.yml --extra-vars "cond_action=undeploy"
elif [ "$choice" = "42" ]
then
    echo "Starting playbook <42 - First Boot Stack Pick to light>"
    $start_cmd 04-stack-ptl.yml --extra-vars "cond_action=first_boot"
#----
else 
    echo "Wrong choice, exit"
fi;
